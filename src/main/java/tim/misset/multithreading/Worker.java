package tim.misset.multithreading;

public class Worker implements Runnable {

    Challenge challenge;
    boolean isFinished = true;
    public Worker(Challenge challenge){
        this.challenge = challenge;
    }

    public void run() {
        challenge.solve();
        isFinished = true;
    }


}
