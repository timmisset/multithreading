package tim.misset.multithreading;

import java.util.ArrayList;
import java.util.List;

public class App {
    private static final List<Challenge> challenges = new ArrayList<>();
    private static final List<Thread> threads = new ArrayList<>();

    static {
        // load the set with challenges to solve
        for(int i = 0; i < 1000; i++) {
            challenges.add(new Challenge("Challenge " + i));
        }
    }

    private static void solveSequentially() {
        long start = System.currentTimeMillis();
        challenges.forEach(Challenge::solve);
        long end = System.currentTimeMillis();
        System.out.println("Finished sequentially in " + ((end - start) / 1000.0) + "seconds");
    }

    private static void solveParallel() {
        long start = System.currentTimeMillis();
        // place all challenges into their own threads
        challenges.forEach(challenge -> {
            Thread thread = new Thread(new Worker(challenges.get(threads.size())));
            thread.start();
            threads.add(thread);
        });

        // wait for all threads to finish
        threads.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        long end = System.currentTimeMillis();
        System.out.println("Finished sequentially in " + ((end - start) / 1000.0) + "seconds");
    }

    public static void main(String[] args) throws InterruptedException {
        solveSequentially();
        solveParallel();
    }

}
