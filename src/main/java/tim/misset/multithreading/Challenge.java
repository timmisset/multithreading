package tim.misset.multithreading;

import static java.lang.Thread.sleep;

public class Challenge {

    private final String id;

    public boolean isSolved = false;

    public Challenge(String id) {
        this.id = id;
    }
    public void solve() { solve(true); }
    public void solve(boolean report) {
        try {
            if(report) { System.out.println("Solving " + id); }
            sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        isSolved = true;
    }

}
